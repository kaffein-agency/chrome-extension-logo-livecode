// Récupérer le logo depuis les settings
chrome.storage.sync.get("logo", function (settings) {
    if(!settings.logo) {
        return;
    }

    const logoImg = document.createElement("img");
    logoImg.src = settings.logo;
    logoImg.style = "height: 20px;"

    console.log("DOM maybe loaded. Logo : " + settings.logo)

    // On document ready, replace the logo
    const interval = setInterval(function () {
        if(document.querySelector("#hs-nav-v4--logo") === null) {
            return;
        }
        clearInterval(interval);
        
        console.log("DOM loaded. Logo : " + settings.logo)
        document.getElementById("hs-nav-v4--logo").replaceWith(logoImg);
    }, 50)
});
