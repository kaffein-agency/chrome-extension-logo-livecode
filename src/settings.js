function save() {
    chrome.storage.sync.set({logo: document.getElementById("logo").value}, function () {
        document.getElementById("message").innerHTML = "Saved";
        setTimeout(function () {
            document.getElementById("message").innerHTML = "";
        }, 3000);
    });
}

chrome.storage.sync.get("logo", function (settings) {
    document.getElementById("logo").value = settings.logo || "";
});


document.getElementById("submit").addEventListener("click", save);